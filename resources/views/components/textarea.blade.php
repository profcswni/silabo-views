@props(['lbl','campo','id', 'data', 'cols' => 10, 'reload' => false])
<div class="col-span-6">
    <x-jet-label for="{{$id}}" value="{{ $lbl }}"/>
    <textarea id="{{$id}}"
              cols="{{$cols}}"
              class="mt-1 block w-full h-80"
              wire:model{{$reload ? '' :'.defer'}}="{{$campo}}"
    >
    </textarea>
    <x-jet-input-error for="{{$campo}}" class="mt-2"/>
</div>

