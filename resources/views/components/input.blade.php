@props(['lbl','campo','id', 'type' => 'text', 'css' => 'col-span-6'])
<div class="{{$css}}">
    <x-jet-label for="{{$id}}" value="{{ $lbl }}"/>
    <x-jet-input id="{{$id}}"
                 type="{{$type}}"
                 class="mt-1 block w-full"
                 wire:model.defer="{{$campo}}"
    />
    <x-jet-input-error for="{{$campo}}" class="mt-2"/>
</div>

