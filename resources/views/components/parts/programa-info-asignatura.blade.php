@props(['lbl', 'value','icon' => 'gmdi-book-r'])
<div class="text-gray-600 dark:text-gray-600 text-xs">{{$lbl}}</div>
<div class="mt-1.5 flex items-center">
    <div class="text-theme-6 flex text-xs font-medium tooltip cursor-pointer">
        @svg($icon,'w-6 text-green-400')
    </div>
    <div class="text-base">{{$value}}</div>

</div>
