@props(['detalles', 'contenidos', 'selected'=> 0, 'sel' => ''])
@if ($detalles->unidad_id)
    <div class="col-span-6 mb-4 w-full md:w-auto">
        <div
            x-data="{ open: false }"
            @keydown.window.escape="open = false"
            x-on:click.away="open = false"
            class="inline-block relative w-full text-left"
        >
            <div>
                <span class="rounded-md shadow-sm">
                    <button
                        x-on:click="open = !open"
                        type="button"
                        class="inline-flex justify-between px-4 py-4 w-full text-gray-700 bg-white rounded-2xl border border-gray-600 shadow-sm hover:bg-gray-50 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                        id="column-select-menu"
                        aria-haspopup="true"
                        x-bind:aria-expanded="open"
                        aria-expanded="true"
                    >
                        <span>@lang('Contenidos de la unidad') ({{$selected}})</span>
<svg class="-mr-1 ml-2 w-5 h-5 float-right" x-description="A"
     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd"
                                  d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                  clip-rule="evenodd"></path>
                        </svg>
                    </button>


                </span>
            </div>

            <div
                x-cloak
                x-show="open"
                x-transition:enter="transition ease-out duration-100"
                x-transition:enter-start="transform opacity-0 scale-95"
                x-transition:enter-end="transform opacity-100 scale-100"
                x-transition:leave="transition ease-in duration-75"
                x-transition:leave-start="transform opacity-100 scale-100"
                x-transition:leave-end="transform opacity-0 scale-95"
                class="absolute right-0 z-50 mt-2 w-full bg-white rounded-md divide-y divide-gray-100 ring-1 ring-black ring-opacity-5 shadow-lg origin-top-right md:w-auto focus:outline-none"
            >
                <div class="bg-white rounded-md shadow-xs dark:bg-gray-700 dark:text-white">
                    <div class="p-2" role="menu" aria-orientation="vertical" aria-labelledby="column-select-menu">
                        @foreach($contenidos as $id => $item)
                            <div wire:key="columnSelect-{{ $loop->index }}">
                                @if(is_array($item))
                                    @foreach($item as $subitem)
                                        @php
                                            if(isset($detalles->contenidos_id)){
                                                if(in_array($subitem->id, $detalles->contenidos_id)){
                                                    $sel = 'checked';
                                                }
                                            }else{$sel ='';}
                                        @endphp.
                                        <label
                                            wire:loading.attr="disabled"
                                            class="inline-flex items-center px-2 py-1 ml-3 disabled:opacity-50 disabled:cursor-wait"
                                        >
                                            <input
                                                class="text-green-800 rounded border-gray-300 shadow-sm transition duration-150 ease-in-out focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 dark:bg-gray-900 dark:text-white dark:border-gray-600 dark:hover:bg-gray-600 dark:focus:bg-gray-600 disabled:opacity-50 disabled:cursor-wait"
                                                wire:model="contenidos_id.id{{$subitem->id}}"
                                                wire:target="contenidos_id.id{{$subitem->id}}"
                                                wire:loading.attr="disabled"
                                                {{$sel}}
                                                type="checkbox"
                                                value="{{ $subitem->id}}"
                                            />
                                            <span class="ml-2">{{ $subitem->data}}</span>
                                        </label>
                                    @endforeach
                                @else
                                    @php
                                        if(isset($detalles->contenidos_id)){
                                                if(in_array($item->id, $detalles->contenidos_id)){
                                                    $sel = 'checked';
                                                }
                                            }else{$sel ='';}
                                    @endphp
                                    <label
                                        wire:loading.attr="disabled"
                                        class="inline-flex items-center px-2 py-1 disabled:opacity-50 disabled:cursor-wait"
                                    >
                                        <input
                                            class="text-indigo-600 rounded border-gray-300 shadow-sm transition duration-150 ease-in-out focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 dark:bg-gray-900 dark:text-white dark:border-gray-600 dark:hover:bg-gray-600 dark:focus:bg-gray-600 disabled:opacity-50 disabled:cursor-wait"
                                            wire:model="contenidos_id.id{{$item->id}}"
                                            wire:target="contenidos_id.id{{$item->id}}"
                                            wire:loading.attr="disabled"
                                            type="checkbox"
                                            {{$sel}}
                                            value="{{ $item->id}}"
                                        />
                                        <span class="ml-2">{{ $item->data}}</span>
                                    </label>
                                @endif

                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
