@props(['lbl', 'data','icon' => 'gmdi-book-r'])
<div class="intro-y">
    <div
        class="inline-block sm:block text-gray-700 dark:text-gray-500 bg-gray-100 dark:bg-dark-1 border-b border-gray-200 dark:border-dark-1">
        <div class="px-0 py-1 flex">
            <div
                class="w-full sm:w-1/3 flex-none flex items-center p-1 sm:rounded-none rounded-t-2xl sm:rounded-l-2xl bg-gray-300">
                <div class="truncate ml-3">{{$lbl}}</div>
            </div>
            <div
                class="w-full sm:w-2/3 sm:w-full p-1 sm:rounded-none rounded-b-2xl sm:rounded-r-2xl bg-gray-200">{!! $data !!}</div>
        </div>
    </div>
</div>
