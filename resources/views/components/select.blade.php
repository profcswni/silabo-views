@props(['lbl','campo','id', 'data', 'reload' => false, 'css' => 'col-span-6'])
<div class="{{$css}}">
    <x-jet-label for="{{$id}}" value="{{ $lbl }}"/>
    <select id="{{$id}}" name="{{$id}}" wire:loading.attr="disabled"
            class="mt-1 block w-full rounded-2xl disabled:opacity-25 transition"
            wire:model{{$reload ? '' :'.defer'}}="{{$campo}}"
    >
        <option value="0">Selecciona una opción</option>
        @if(json_decode($data))
            @foreach(json_decode($data) as $value)
                @if(isset($value->nombre))
                    @if(isset($value->fecha))
                        <option value="{{$value->id}}">{{$value->nombre}}
                            - {{\Carbon\Carbon::make($value->fecha)->format('d-m-Y')}}</option>
                    @else
                        <option value="{{$value->id}}">{{$value->nombre}}</option>
                    @endif

                @elseif(isset($value->name))
                    <option value="{{$value->id}}">{{strip_tags($value->name)}}</option>
                @elseif(isset($value->data))
                    <option value="{{$value->id}}">{{strip_tags($value->data)}}</option>
                @elseif(isset($value->grupo_id))
                    <option value="{{$value->id}}">
                        Año: {{strip_tags($value->grupo->nombre)}} -
                        Aula: {{strip_tags($value->aula->nombre)}} -
                        Asignatura: {{strip_tags($value->asignatura->nombre)}} -
                        Bloque: {{strip_tags($value->bloque->nombre)}}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
    <x-jet-input-error for="{{$campo}}" class="mt-2"/>
</div>

