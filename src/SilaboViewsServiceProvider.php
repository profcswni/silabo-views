<?php

namespace Cswni\SilaboViews;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class SilaboViewsServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'silabo');

        $this->callAfterResolving(BladeCompiler::class, function () {
            $this->registerComponent('form');
            $this->registerComponent('input');
            $this->registerComponent('select');
            $this->registerComponent('textarea');
        });

    }

    /**
     * Register the given component.
     *
     * @param string $component
     * @return void
     */
    protected function registerComponent(string $component)
    {
        Blade::component('components.' . $component, 'silabo-' . $component, 'uml');
    }
}
